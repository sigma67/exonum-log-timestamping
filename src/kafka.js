import * as proto from "../proto/stubs";

/***************************
 * KAFKA consumer submitting all incoming
 * first argument: KAFKA IP address
 * second argument: EXONUM IP address and port
 */


let kafka = require("kafka-node")
let Exonum = require('exonum-client')
let CryptoJS = require('crypto-js')
let config = require('../config');

const SERVICE_ID = 130
const MESSAGE_ID = 0

let privateKey = config.privateKey
let publicKey = config.publicKey
let topic_name = "logs"

/**
 * Process command line arguments
 */
var args = process.argv.slice(2);
if(args.length < 2){
    console.log("Not enough arguments provided.");
}
let kafka_address = args[0];
let exonum_address = args[1];


/**
 * Consumer setup
 */
const client = new kafka.Client(kafka_address);
const topics = [
    { topic: topic_name }
];
const options = { fromOffset: 'earliest' };

const consumer = new kafka.Consumer(client, topics, options);

/**
 * Processing loop for incoming messages
 */
consumer.on("message", function(message) {
    postTransaction(exonum_address, message.value);
});

function postTransaction(api, payload){

    var hash;
    try {
        hash = CryptoJS.algo.SHA256.create();
        hash.update(CryptoJS.enc.Latin1.parse(payload));

        hash = hash.finalize().toString();

    } catch (error) {
        reject(error)
    }

    const transaction = Exonum.newTransaction({
        author: publicKey,
        service_id: SERVICE_ID,
        message_id: MESSAGE_ID,
        schema: proto.exonum.examples.timestamping.TxTimestamp
    });

    // Transaction data
    const data = {
        content: {
            content_hash: { data: Exonum.hexadecimalToUint8Array(hash) },
            metadata: topic_name
        }
    }

    return transaction.send('/api/explorer/v1/transactions', data, privateKey)
}

consumer.on("error", function(err) {
    console.log("error", err);
});

process.on("SIGINT", function() {
    consumer.close(true, function() {
        process.exit();
    });
});