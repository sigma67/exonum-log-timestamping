let proto = require("../proto/stubs")

let express = require('express');
let router = express.Router();
let config = require('../config');

let Exonum = require('exonum-client')
let CryptoJS = require('crypto-js')
let uniqid = require('uniqid')

const SERVICE_ID = 130
const MESSAGE_ID = 0

let privateKey = config.privateKey
let publicKey = config.publicKey
let topic_name = "infrastruktur-firewall-system-syslog"


/* GET home page.  CHANGE TO POST */
router.post('/ts', function (req, res, next) {

    var max = req.body.count;
    var count = 0;

    while(count < max){
        count++;
        if(count > max){
            return;
        }

        var line = uniqid(); //ensure uniqueness for testing purposes

        postTransaction(req.app.get('apiRoot'), line);
    }

    res.set('Content-Type', 'text/html')
    res.send(new Buffer("running"));
});

router.get('/keypair', function (req, res, next) {

    const keys = Exonum.keyPair();
    res.set('Content-Type', 'text/html')
    res.send(new Buffer(keys.secretKey + "<br>" + keys.publicKey));
});


function postTransaction(api, payload){

    var hash;
    try {
        hash = CryptoJS.algo.SHA256.create();
        hash.update(CryptoJS.enc.Latin1.parse(payload));

        hash = hash.finalize().toString();

    } catch (error) {
        reject(error)
    }

    const transaction = Exonum.newTransaction({
        author: publicKey,
        service_id: SERVICE_ID,
        message_id: MESSAGE_ID,
        schema: proto.exonum.examples.timestamping.TxTimestamp
    });

    // Transaction data
    const data = {
        content: {
            content_hash: { data: Exonum.hexadecimalToUint8Array(hash) },
            metadata: topic_name
        }
    }

    return transaction.send('/api/explorer/v1/transactions', data, privateKey)
}

module.exports = router;