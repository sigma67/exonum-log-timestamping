#!/usr/bin/env bash

ips=HOSTS

for i in {1..60}
do
    for peer in `cat $ips`; do
     peer=${peer//$'\r'/}
     curl -X POST "http://"${peer}":2268/ts" -d '{"count": 250}' -H 'Content-type:application/json' &
    done
    sleep 1
done
