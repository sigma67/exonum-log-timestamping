#!/usr/bin/env bash
ips=HOSTS

NP=$((`wc -l $ips | cut -d ' ' -f 1`+1))
echo "Starting nodes ($NP) ..."

i=1
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}

    scp -o "StrictHostKeyChecking no" -i awsoregon.key node$i"/consensus.key.toml" "root@"$peer:consensus.key.toml &
    scp -o "StrictHostKeyChecking no" -i awsoregon.key node$i"/service.key.toml" "root@"$peer:service.key.toml &

    sed -i "2s#.*#consensus_secret_key = 'consensus.key.toml'#" node$i"/cfg.toml"
    sed -i "6s#.*#service_secret_key = 'service.key.toml'#" node$i"/cfg.toml"
    scp -o "StrictHostKeyChecking no" -i awsoregon.key node$i"/cfg.toml" "root@"$peer:node.toml &
    let "i=i+1"
done

echo "Waiting 3 seconds for uploads to finish"
sleep 3

i=1
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}
    ssh -o "StrictHostKeyChecking no" -i awsoregon.key "root@"$peer "chmod 600 consensus.key.toml service.key.toml" &
done

echo "Waiting 2 seconds for uploads to finish"
sleep 2

i=1
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}
    ssh -o "StrictHostKeyChecking no" -i awsoregon.key "root@"$peer "sudo /root/.cargo/bin/exonum-timestamping run --node-config node.toml --db-path db --public-api-address 0.0.0.0:8200 --consensus-key-pass pass:a --service-key-pass pass:a &" &
    #ssh -o "StrictHostKeyChecking no" -i awsoregon.key "root@"$peer "cd /usr/src/exonum-log-timestamping && nohup npm start &" &
    let "i=i+1"
done

echo "($NP) nodes successfully started."