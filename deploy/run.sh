#!/usr/bin/env bash

cd /usr/src/exonum-log-timestamping &&  nohup npm start -- --port=2268 --api-root=http://127.0.0.1:8200 >> app.log 2>&1 &
/root/.cargo/bin/exonum-timestamping run --node-config node.toml --db-path /usr/src/exonum/examples/timestamping/backend/example/db

rm -r /usr/src/exonum/examples/timestamping/backend/example/db
/root/.cargo/bin/exonum-timestamping run --node-config node.toml --db-path db
cd /usr/src/exonum-log-timestamping/ && git pull && cd ~
cd /usr/src/exonum-log-timestamping/ && npm run build && cd ~
