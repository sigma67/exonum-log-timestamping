#!/usr/bin/env bash
ips=HOSTS
time=20

NP=$((`wc -l $ips | cut -d ' ' -f 1`+1))

for peer in `cat $ips`; do
 peer=${peer//$'\r'/}
 ssh -o "StrictHostKeyChecking no" -i awsoregon.key "root@"${peer} "sudo /root/.cargo/bin/exonum-bench $((12000/NP)) ${time} 1000 &" &
done