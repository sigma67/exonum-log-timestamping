#!/usr/bin/env bash
i=0

apt-get update && apt-get install -y software-properties-common && add-apt-repository ppa:maarten-fonville/protobuf -y && apt-get update && apt-get install -y curl git gcc cpp libssl-dev pkg-config libsodium-dev libsnappy-dev librocksdb-dev libprotobuf-dev protobuf-compiler clang
curl https://sh.rustup.rs -sSf | sh -s -- -y  --default-toolchain=stable
curl -sL https://deb.nodesource.com/setup_8.x | bash && apt-get install -y nodejs
cd /usr/src
git clone --depth 1 https://github.com/sigma67/exonum.git -b performance && mv /root/.cargo/bin/* /usr/bin && cd exonum/examples/timestamping/backend && cargo update && cargo install  --path .
cd ../benchmark && cargo install  --path .
cd /usr/src
git clone https://bdsuev:SjXfj5nMakeCGTs2nyZK@bitbucket.org/bdsuev/exonum-log-timestamping
cd exonum-log-timestamping && npm install && npm run build
cd /usr/src/exonum-log-timestamping