#!/usr/bin/env bash

ips=HOSTS

NP=$((`wc -l $ips | cut -d ' ' -f 1`+1))

rm -r node*

echo "Generating config for ($NP) nodes"

#base template
#~/.cargo/bin/exonum-timestamping generate-template common.toml --validators-count $NP
sed -i "12s#.*#validators_count = $NP#" common.toml

i=1
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}
    ~/.cargo/bin/exonum-timestamping generate-config common.toml node${i} --peer-address $peer:6331 --consensus-key-pass pass:a --service-key-pass pass:a
    let i=$i+1
done

i=1
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}
    ~/.cargo/bin/exonum-timestamping finalize --public-api-address 0.0.0.0:8200 --private-api-address 0.0.0.0:8091 node$i/sec.toml node$i"/cfg.toml" --public-configs $(ls node*/pub.toml )
    let i=$i+1
done

#tar czf - keys | ssh -o "StrictHostKeyChecking no" -i $peer".key" $CONFIG "ubuntu@"$peer tar -xvzf -C ~/keys