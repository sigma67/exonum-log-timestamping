#!/usr/bin/env bash

curl -X POST http://localhost:8091/api/system/v1/shutdown -H 'Content-type:application/json' -d 'null'
curl -X POST http://localhost:8092/api/system/v1/shutdown -H 'Content-type:application/json' -d 'null'
curl -X POST http://localhost:8093/api/system/v1/shutdown -H 'Content-type:application/json' -d 'null'
curl -X POST http://localhost:8094/api/system/v1/shutdown -H 'Content-type:application/json' -d 'null'