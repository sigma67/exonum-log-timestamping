#!/usr/bin/env bash

ips=HOSTS
#reset peers - shutdown and delete database
for peer in `cat $ips`; do
    peer=${peer//$'\r'/}
    echo "connecting to ${peer}..."
    curl -X POST http://$peer:8091/api/system/v1/shutdown -H 'Content-type:application/json' -d 'null'
    ssh -o "StrictHostKeyChecking no" -i "awsoregon.key" "root@"$peer "rm -r db"
done